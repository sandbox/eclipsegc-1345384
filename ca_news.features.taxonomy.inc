<?php
/**
 * @file
 * ca_news.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ca_news_taxonomy_default_vocabularies() {
  return array(
    'ca_news_topics' => array(
      'name' => 'News Topics',
      'machine_name' => 'ca_news_topics',
      'description' => 'A vocabulary of terms for news articles.',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
