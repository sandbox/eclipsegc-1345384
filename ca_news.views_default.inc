<?php
/**
 * @file
 * ca_news.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ca_news_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'ca_news';
  $view->description = 'A view of news article in reverse chronological order.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Context Admin News';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = 1;
  $handler->display->display_options['row_options']['comments'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'ca_news' => 'ca_news',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_title'] = 'Context Admin News';
  $export['ca_news'] = $view;

  $view = new view;
  $view->name = 'news_images';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'news images';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_ca_news_image']['id'] = 'field_ca_news_image';
  $handler->display->display_options['fields']['field_ca_news_image']['table'] = 'field_data_field_ca_news_image';
  $handler->display->display_options['fields']['field_ca_news_image']['field'] = 'field_ca_news_image';
  $handler->display->display_options['fields']['field_ca_news_image']['label'] = '';
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ca_news_image']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_ca_news_image']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ca_news_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_ca_news_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_ca_news_image']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Contextual filter: Content: Article (field_ca_news_article_ref) */
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['id'] = 'field_ca_news_article_ref_nid';
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['table'] = 'field_data_field_ca_news_article_ref';
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['field'] = 'field_ca_news_article_ref_nid';
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['default_argument_type'] = 'php';
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['default_argument_options']['code'] = 'return arg(1);';
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['field_ca_news_article_ref_nid']['not'] = 0;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'ca_news_image' => 'ca_news_image',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['argument_input'] = array(
    'field_ca_news_article_ref_nid' => array(
      'type' => 'context',
      'context' => 'node_edit_form.nid',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Content: Article (field_ca_news_article_ref)',
    ),
  );
  $export['news_images'] = $view;

  return $export;
}
